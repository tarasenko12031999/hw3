const fs = require('fs');
const path = require('path');


fs.watch(path.join(__dirname, './dist'), (eventType, fileName) => {
    const fileThatWasChanged = path.join(__dirname, './dist', fileName);

    if (fs.existsSync(fileThatWasChanged)) {
        const content = fs.readFileSync(fileThatWasChanged);
        const parsedData = JSON.parse(content.toString());
        fs.unlinkSync(fileThatWasChanged);
        switch (parsedData.type) {
            case 'gas':
                fs.appendFile(
                    path.join(__dirname, './audit/gas.csv'), `\n${parsedData.date},${parsedData.value},${parsedData.name}`,
                    () => {
                        console.log('written');
                    }
                );
                break;
            case 'water':
                fs.appendFile(
                    path.join(__dirname, './audit/water.csv'), `${parsedData.date},${parsedData.value},${parsedData.name}\n`,
                    () => {
                        console.log('written');
                    }
                );
                break;
        }
    }
})



/*const fs = require(`fs`);
const path = require(`path`)

fs.watch(path.join(__dirname, `./dist`), (evenType, fileName) => {
    const fileThatWasChanged = path.join(__dirname, `./dist`, fileName);

    const content = fs.readFileSync(fileThatWasChanged);

    const parseData = JSON.parse(content.toString());
    switch(parseData.type) {
        case `gas`:
            fs.appendFile(
                path.join(__dirname, `./audit/gas.csv`, `\n${parseData.date},${parseData.value},${parseData.name}`),
                () => console.log(`written`));
            break;
    }




})*/